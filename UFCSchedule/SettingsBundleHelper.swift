//
//  SettingsBundleHelper.swift
//  UFCSchedule
//
//  Created by Jay on 9/28/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SettingsBundleHelper {
    struct SettingsBundleKeys {
        static let Reset = "RESET_SAVED_DATA"
        static let BuildVersionKey = "build_preference"
        static let AppVersionKey = "version_preference"
    }
    class func checkAndExecuteSettings() {
        if UserDefaults.standard.bool(forKey: SettingsBundleKeys.Reset) {
            UserDefaults.standard.set(false, forKey: SettingsBundleKeys.Reset)
            let appDomain: String? = Bundle.main.bundleIdentifier
            UserDefaults.standard.removePersistentDomain(forName: appDomain!)
            UserDefaults.standard.synchronize()
            
            URLCache.shared.removeAllCachedResponses()

            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UFCEvent")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName: "UFCEventFight")
            let deleteRequest2 = NSBatchDeleteRequest(fetchRequest: fetchRequest2)
            
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let context = delegate.persistentContainer.viewContext
            
            do {
                try context.persistentStoreCoordinator!.execute(deleteRequest2, with: context)
                try context.persistentStoreCoordinator!.execute(deleteRequest, with: context)
                try context.save()
            } catch _ as NSError {
            }
        }
    }
    
    class func setVersionAndBuildNumber() {
        let version: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        UserDefaults.standard.set(version, forKey: "version_preference")
        let build: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        UserDefaults.standard.set(build, forKey: "build_preference")
    }
}

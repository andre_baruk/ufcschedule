//
//  UFCEvent+CoreDataProperties.swift
//  UFCSchedule
//
//  Created by Jay on 9/12/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

extension UFCEvent {
    @NSManaged public var arena: String
    @NSManaged public var baseTitle: String
    @NSManaged public var etTime: String
    @NSManaged public var eventDate: Date
    @NSManaged public var eventEndDate: Date
    @NSManaged public var eventId: Int32
    @NSManaged public var eventStatus: String
    @NSManaged public var location: String
    @NSManaged public var subTitle: String
    @NSManaged public var eventFights: Set<UFCEventFight>
    @NSManaged public var isEventThisWeekValue: NSNumber
    @NSManaged public var localFullDateTimeDescription: String
    
    @objc(addEvents:)
    @NSManaged public func addToEvents(_ values: NSSet)
    
    @objc(addUFCEventObject:)
    @NSManaged public func addToUFCEvents(_ value: UFCEventFight)
}

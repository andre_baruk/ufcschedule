//
//  UFCEventFight+CoreDataProperties.swift
//  UFCSchedule
//
//  Created by Jay on 9/12/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//
//

import Foundation
import CoreData


extension UFCEventFight {
    @NSManaged public var eventId: Int32
    @NSManaged public var fightId: Int32
    @NSManaged public var fighter1Id: Int32
    @NSManaged public var fighter2Id: Int32
    @NSManaged public var fighter1FirstName: String
    @NSManaged public var fighter1LastName: String
    @NSManaged public var fighter2FirstName: String
    @NSManaged public var fighter2LastName: String
    @NSManaged public var weightClass: String
    @NSManaged public var ufcEvent: UFCEvent?
    @NSManaged public var order: Int32
    @NSManaged public var isMainEventValue: NSNumber
}

//
//  UFCEvent+Extensions.swift
//  UFCSchedule
//
//  Created by Jay on 9/27/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//
import Foundation
import CoreData
import UIKit

extension UFCEvent {

    // MARK: - Static Properties
    
    private static var context = CoreDataHelper.sharedInstance.container.viewContext

    // MARK: - Computed Properties
    
    var isEventThisWeek: Bool {
        get {
            return isEventThisWeekValue.boolValue
        }
        set(newValue) {
            isEventThisWeekValue = NSNumber.init(booleanLiteral: newValue)
        }
    }
    
    var localDateString: String {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            return dateFormatter.string(from: eventDate)
        }
    }
    
    var formattedGMTDateString: String {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.timeZone = TimeZone(identifier:"GMT")
            return dateFormatter.string(from: eventDate)
        }
    }
    
    var isEventOneDayOld: Bool {
        get {
            let hoursSinceEnd = Calendar.current.dateComponents([.hour], from: self.eventEndDate, to: Date.init()).hour!
            return (hoursSinceEnd <= 24 && hoursSinceEnd >= 0)
        }
    }
    
    var orderedEventFights: [UFCEventFight] {
        var array = Array(self.eventFights)
        array.sort(by: { $0.order < $1.order })
        return array
    }
    
    // MARK: - Data Setup Methods
    
    convenience init(_ dictionary: [String: Any]) {
        self.init(context: UFCEvent.context)
        updateInfo(dictionary)
    }
    
    func updateInfo(_ dictionary: [String: Any]) {
        self.eventId = dictionary["id"] as? Int32 ?? 0
        self.baseTitle = dictionary["base_title"] as? String ?? ""
        self.subTitle = dictionary["title_tag_line"] as? String ?? ""
        self.eventStatus = dictionary["event_status"] as? String ?? ""
        self.arena = dictionary["arena"] as? String ?? ""
        self.location = dictionary["location"] as? String ?? ""
        self.etTime = getETTime(ETPT: dictionary["event_time_text"] as? String ?? "")
        self.eventDate = UFCEvent.getDate(dictionaryDateEntry: dictionary["event_dategmt"] as? String ?? "")!
        self.eventEndDate = UFCEvent.getDate(dictionaryDateEntry: dictionary["end_event_dategmt"] as? String ?? "")!
        self.localFullDateTimeDescription = getFullLocalDateTime()
        self.isEventThisWeekValue = CheckIfEventIsCurrentWeek() as NSNumber
    }
    
    public class func saveDataToContext() {
        context.performAndWait {
            try! context.save()
        }
    }
    
    // MARK: - Data Formatting Methods
    
    class func getDate(dictionaryDateEntry date: String) -> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        dateFormatter.timeZone = TimeZone(identifier:"GMT")
        return dateFormatter.date(from: date as String)
    }
    
    func getFullLocalDateTime() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var tempString = dateFormatter.string(from: self.eventDate)
        dateFormatter.dateFormat = "hh:mm a"
        tempString = ("\(tempString) at \(dateFormatter.string(from: self.eventDate))")
        return tempString
    }
    
    func getETTime(ETPT: String) -> String {
        if (ETPT.count > 0)
        {
            //print(ETPT)
            return String(ETPT.split(separator: "/")[0])
        }
        else
        {
            return ""
        }
    }
    
    func getConvertedTime(originTime: String) -> String {
        
        var dateFormatString = ""
        let originTimeCopy = String(format: originTime)
        if (originTimeCopy.split(separator: ":").count == 2) {
            dateFormatString = "hh:mma"
        }
        else {
            dateFormatString = "ha"
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatString
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        let date : Date? = dateFormatter.date(from: originTime)
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = dateFormatString
        
        guard let _: Date = date else {
            return "N/A"
        }
        
        let dateString = outputDateFormatter.string(from: date!)
        return dateString
    }
    
    func CheckIfEventIsCurrentWeek() -> Bool {
        let daysToEvent = Calendar.current.dateComponents([.day], from: Date.init(), to: self.eventDate).day
        if (daysToEvent! < 7)
        {
            return true
        }
        return false
    }
    
    func GetTimeUntilEvent() -> String{
        
        let dateNow = Date.init()
        var seconds = Calendar.current.dateComponents([.second], from: dateNow, to: self.eventDate).second!
        var minutes = seconds / 60
        seconds -= (minutes * 60)
        var hours = minutes / 60
        minutes -= (hours * 60)
        let days = hours / 24
        hours -= (days * 24)
        
        if (days > 0)
        {
            return "\(days) days \(hours) hours"
        }
        else if (hours > 0)
        {
            return "\(hours) hours \(minutes) min"
        }
        else if (minutes > 0)
        {
            return "\(minutes) min \(seconds) sec"
        }
        else if (seconds > 0)
        {
            return "\(seconds) sec"
        }
        else
        {
            return  "We Are Live!"
        }
    }
    
    func IsEventFinished() -> Bool {
        
        // returns true if no time left for the event
        return Calendar.current.dateComponents([Calendar.Component.second], from: Date.init(), to: self.eventEndDate).second! <= 0
    }
    
    //
    // FOR TESTING PURPOSES
    //
    func GetTestDate(hourOffset: Int = 0, minuteOffset: Int = 0, secondOffset: Int = 0) -> Date {
        
        var returnDate = Date.init()
        
        returnDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: returnDate)!
        returnDate = Calendar.current.date(byAdding: .minute, value: minuteOffset, to: returnDate)!
        returnDate = Calendar.current.date(byAdding: .second, value: secondOffset, to: returnDate)!
        
        return returnDate
    }
    
    
    // MARK: - Fetching Methods
    
    @nonobjc private class func fetchUFCEventRequest() -> NSFetchRequest<UFCEvent> {
        return NSFetchRequest<UFCEvent>(entityName: "UFCEvent")
    }
    
    @nonobjc private class func fetchSingleUFCEventRequest(eventId: Int) -> NSFetchRequest<UFCEvent> {
        let fetchRequest : NSFetchRequest<UFCEvent> = UFCEvent.fetchRequest() as! NSFetchRequest<UFCEvent>
        fetchRequest.predicate = NSPredicate.init(format: "eventId == %d", eventId)
        return fetchRequest
    }
    
    @nonobjc public class func fetchUFCEvents(completion: @escaping ([UFCEvent]) -> () = { _ in }) {
        let fetch = self.fetchUFCEventRequest()
 
        var entities: [UFCEvent] = []
        context.perform {
            entities = try! fetch.execute()
            entities.sort(by: { $0.eventDate < $1.eventDate })
            completion(entities)
        }
    }
    
    @nonobjc public class func fetchUFCEventsAndWait(completion: @escaping ([UFCEvent]) -> () = { _ in }) {
        let fetch = self.fetchUFCEventRequest()
        
        var entities: [UFCEvent] = []
        context.performAndWait {
            entities = try! fetch.execute()
            entities.sort(by: { $0.eventDate < $1.eventDate })
            completion(entities)
        }
    }
    
    @nonobjc public class func fetchUFCEvent(byId eventId: Int, completion: @escaping (UFCEvent?) -> () = { _ in }) {
        let fetch = self.fetchSingleUFCEventRequest(eventId: eventId)
        
        context.performAndWait {
            let entity = try! fetch.execute().first
            completion(entity)
        }
    }
    
    public override func awakeFromFetch() {
        super.awakeFromFetch()
        isEventThisWeekValue = self.CheckIfEventIsCurrentWeek() as NSNumber
    }
    
    // MARK: - Data Removal Methods
    
    public class func checkForOldEventsAndRemoveWithChildEntites(byEvents events: inout [UFCEvent]) {
        
        var didRemoveAnyEvent = false
        for event in events {
            if Calendar.current.dateComponents([.day], from: Date.init(), to: event.eventDate).day! < 0 {
                for fight in event.eventFights {
                    context.delete(fight)
                }
                events.removeAll(where: {$0.eventId == event.eventId})
                context.delete(event)
                didRemoveAnyEvent = true
            }
        }
        
        if didRemoveAnyEvent {
            do {
                try context.save()
            } catch let error {
                print(error)
            }
        }
    }
    
    
    // MARK: - API Calling Methods
    
    public class func getAndSaveNewUFCEventsFromAPI() -> [UFCEvent] {
        let events = UFCEvent.getUFCEventsFromAPI()
        context = (events.first?.managedObjectContext!)!
        saveDataToContext()
        return events
    }
    
    public class func updateExistingEventsWithFreshDataFromAPI(events: [UFCEvent]) -> [UFCEvent] {
       // var events: [UFCEvent] = []
        
       // UFCEvent.fetchUFCEventsAndWait (completion: { entities in
         //   events = entities
       // })
        var events = events
        
        let eventsData = APICaller.getUFCEventsData() { jsonDataModel in
            return jsonDataModel.filter( {
                return (getDate(dictionaryDateEntry: $0["event_dategmt"] as! String)! > Date.init().addingTimeInterval(-1)) &&
                    ($0["event_status"] as! String) == "FINALIZED"
            })
        }
        
        if eventsData.count > 0 {
            for eventData in eventsData {
                let event = events.filter({
                    (eventData.filter({ (($0.key as String) == "id")})
                        .first?.value as? Int)?.description == $0.eventId.description
                }).first
                
                if let event = event {
                    event.updateInfo(eventData)
                } else{
                    events.append(UFCEvent.init(eventData))
                }
            }
            UFCEvent.saveDataToContext()
            events.sort(by: {$0.eventDate < $1.eventDate })
        } else {
            events.removeAll()
        }
        return events
    }
    
    public class func getUFCEventsFromAPI() -> [UFCEvent] {
        var events: [UFCEvent] = []
        let eventsData = APICaller.getUFCEventsData() { jsonDataModel in
            return jsonDataModel.filter( {
                return (getDate(dictionaryDateEntry: $0["event_dategmt"] as! String)! > Date.init().addingTimeInterval(-1)) &&
                    ($0["event_status"] as! String) == "FINALIZED"
            })
        }
        
        if eventsData.count > 0 {
            for eventData in eventsData {
                let event = UFCEvent.init(eventData)

                events.append(event)
            }
            events.sort(by: {$0.eventDate < $1.eventDate })
        }
        
        return events
    }
}

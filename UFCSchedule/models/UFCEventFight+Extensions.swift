//
//  UFCEventFight+Extensions.swift
//  UFCSchedule
//
//  Created by Jay on 9/30/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension UFCEventFight {
    
    // MARK: - Static Properties
    
    private static var context = CoreDataHelper.sharedInstance.container.viewContext
    
    private static var _fightersData: [[String : Any]]? = nil
    private static var fightersData: [[String : Any]] {
        
        get {
        if _fightersData == nil {
            _fightersData = APICaller.getUFCFightersData()
        }
        return _fightersData!
        }
        set (newValue) {
            _fightersData = newValue
        }
    }
    
    // MARK: - Computed Properties
    
    var fighter1Name: String {
        return getFullName(firstN: fighter1FirstName, lastN: fighter1LastName)
    }
    
    var fighter2Name: String {
        return getFullName(firstN: fighter2FirstName, lastN: fighter2LastName)
    }
    
    var fighter1VSfighter2: String {
        return "\(self.fighter1Name) vs \(self.fighter2Name)"
    }
    
    var isMainEvent: Bool {
        return isMainEventValue.boolValue
    }
    
    // MARK: - Data Setup Methods
    
    convenience init(_ dictionary: [String: Any], event: UFCEvent) {
        self.init(context: UFCEventFight.context)
        self.ufcEvent = event
        updateInfo(dictionary)
    }
    
    func updateInfo(_ dictionary: [String: Any]) {
        self.eventId = Int32(dictionary["event_id"] as? Int ?? 0)
        self.fightId = Int32(dictionary["id"] as? Int ?? 0)
        self.fighter1Id = Int32(dictionary["fighter1_id"] as? Int ?? 0)
        self.fighter2Id = Int32(dictionary["fighter2_id"] as? Int ?? 0)
        self.weightClass = dictionary["fighter1_weight_class"] as? String ?? ""
        self.order = Int32(dictionary["fightcard_order"] as? Int ?? 0)
        self.isMainEventValue = NSNumber.init(booleanLiteral:
            dictionary["is_main_event"] as? Bool ?? true)
        
        let fighter1 = UFCEventFight.getFighterData(by: (dictionary["fighter1_id"] as? Int ?? 0))
        let fighter2 = UFCEventFight.getFighterData(by: (dictionary["fighter2_id"] as? Int ?? 0))
        
        if let fighter1 = fighter1 {
            self.fighter1FirstName = fighter1["first_name"] as? String ?? ""
            self.fighter1LastName = fighter1["last_name"] as? String ?? ""
        } else {
            let fighterData = APICaller.getUFCFighterData(by: Int(self.fighter1Id))
            
        }
        
        if let fighter2 = fighter2 {
            self.fighter2FirstName = fighter2["first_name"] as? String ?? ""
            self.fighter2LastName = fighter2["last_name"] as? String ?? ""
        } else {
            // TODO GET FIGHTER BY API ID
            print("not")
        }
        
    }
    
    public class func saveDataToContext() {
        context.performAndWait {
            do {
            try context.save()
            } catch let error {
                print(error)
            }
        }
    }
    
    // MARK: - Fetching Methods
    
    @nonobjc private class func fetchUFCEventFightsRequest(eventId: Int) -> NSFetchRequest<UFCEventFight> {
        let fetchRequest : NSFetchRequest<UFCEventFight> = UFCEventFight.fetchRequest() as! NSFetchRequest<UFCEventFight>
        fetchRequest.predicate = NSPredicate.init(format: "eventId == %d", eventId)
        return fetchRequest
    }
    
    @nonobjc public class func fetchUFCEventFights(eventId: Int, completion: @escaping ([UFCEventFight]) -> () = { entities in
            var entities = entities as [UFCEventFight]
        entities.sort(by: { $0.order > $1.order })
        }) {
        let fetch = self.fetchUFCEventFightsRequest(eventId: eventId)
        
        var entities: [UFCEventFight] = []
        context.perform {
            entities = try! fetch.execute()
            completion(entities)
        }
    }
    
    @nonobjc public class func fetchUFCEventFightsAndWait(eventId: Int, completion: @escaping ([UFCEventFight]) -> () = { _ in }) {
        let fetch = self.fetchUFCEventFightsRequest(eventId: eventId)
        
        var entities: [UFCEventFight] = []
        context.performAndWait {
            entities = try! fetch.execute()
            completion(entities)
        }
    }
    
    // MARK: - Data Formatting Methods
    
    private func getFullName(firstN: String, lastN: String) -> String
    {
        return firstN + " " + lastN
    }
    
    private class func getDate(dictionaryDateEntry date: String) -> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        dateFormatter.timeZone = TimeZone(identifier:"GMT")
        return dateFormatter.date(from: date as String)
    }
    
    private class func getFighterData(by fighterId: Int) -> [String : Any]? {
        return fightersData.filter({ ($0["id"] as? Int) == fighterId }).first
    }
    
    // MARK: - API Calling Methods
    
    public class func getNewUFCFightsFromAPI(event: UFCEvent) -> [UFCEventFight] {
        let fights = UFCEventFight.getUFCFightsFromAPI(event: event)
        return fights
    }
    
    /*
    public class func getNewUFCFightsFromAPI(eventId: Int) -> [UFCEventFight] {
        let fights = UFCEventFight.getUFCFightsFromAPI(event: event)
        return fights
    }
    */
    
    public class func getAndSaveNewUFCFightsFromAPI(event: UFCEvent){
        UFCEventFight.getUFCFightsFromAPI(event: event)
        UFCEventFight.saveDataToContext()
    }
    
    public class func getUFCEventFightsFromAPIAndUpdateExisting(event: UFCEvent) -> [UFCEventFight] {
        var fights: [UFCEventFight] = []
        
        UFCEventFight.fetchUFCEventFightsAndWait (eventId: Int(event.eventId), completion: { entities in
            fights = entities
        })
        
        let fightsData = APICaller.getUFCEventFightsData(by: Int(event.eventId))
        
        if fightsData.count > 0 {
            
            var fight: UFCEventFight?
            
            for fightData in fightsData {
                fight = fights.filter({
                    (fightData.filter({ (($0.key as String) == "id")})
                        .first?.value as? Int)?.description == $0.fightId.description
                }).first
                
                if let fight = fight {
                    fight.updateInfo(fightData)
                } else{
                      let currentEvent = event
                      fights.insert(UFCEventFight.init(fightData, event: currentEvent), at: fights.endIndex)
                }
                UFCEventFight.saveDataToContext()
            }
        } else {
            fights = []
        }
        
        return fights
    }
    
    private class func getUFCFightsFromAPI(event: UFCEvent) -> [UFCEventFight] {
        var fights: [UFCEventFight] = []
        let fightsData = APICaller.getUFCEventFightsData(by: Int(event.eventId))

        if fightsData.count > 0 {
            for fightData in fightsData {
                let fight = UFCEventFight.init(fightData, event: event)
                fights.append(fight)
                fights = fights.sorted(by: { $0.order < $1.order })
            }
        }
        return fights
    }
}

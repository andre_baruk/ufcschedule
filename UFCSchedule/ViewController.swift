//
//  ViewController.swift
//  UFCSchedule
//
//  Created by Jay on 8/17/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit
import ParallaxHeader

class ViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var eventsTableView: UITableView!
    var refreshControl = UIRefreshControl()
    let cellReuseIdentifier = "eventsCell"
    var ufcEvents: [UFCEvent] = []
    var upcomingUFCEvent: UFCEvent!
    var upcomingUFCEventCell: MainPageViewCell!
    
    // MARK: - View Methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        //  Initial table data setup
        eventsTableView.register(UINib(nibName: "MainPageCellView", bundle: nil), forCellReuseIdentifier: "eventsCell")
        eventsTableView.register(UINib(nibName: "MainPageCellView", bundle: nil), forCellReuseIdentifier: "firstCell")
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        
        self.view.backgroundColor = .ufcDarkRed
        
        //  Pull down to refresh setup
        refreshControl.addTarget(self, action: #selector(refreshView(sender:)), for: UIControl.Event.valueChanged)
        eventsTableView.addSubview(refreshControl)
        
        //  Parallax Setup
        let imageView = UIImageView()
        imageView.image = UIImage(named: "arena")
        imageView.contentMode = .scaleAspectFill
        
        eventsTableView.parallaxHeader.view = imageView
        eventsTableView.parallaxHeader.height = 200
        eventsTableView.parallaxHeader.minimumHeight = 0
        eventsTableView.parallaxHeader.mode = .topFill
        
        setupUFCEventsData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        SettingsBundleHelper.checkAndExecuteSettings()
    }
    
    // MARK: - Timer Methods
    
    @objc func updateTimerWithCountdown(timer: Timer?) {
        
        let timeUntilString = upcomingUFCEvent.GetTimeUntilEvent()
        if (timeUntilString != "We Are Live!") {
            upcomingUFCEventCell.countdownLabel.text = ("Live in: \(timeUntilString)")
            upcomingUFCEventCell.backgroundColor = .ufcSelectedRed
        } else {
            if let timer = timer {
                timer.invalidate()
            }
            
            Timer.scheduledTimer(timeInterval: 1800.0, target: self, selector: #selector(updateTimerWhenLiveOrFinished(timer:)), userInfo: nil, repeats: true)
            
            performLiveOrFinishedCountdownTextUpdate()
        }
    }
    
    @objc func updateTimerWhenLiveOrFinished(timer: Timer) {
        performLiveOrFinishedCountdownTextUpdate(completionWhenFinished: {
            timer.invalidate()
        })
    }
    
    func performLiveOrFinishedCountdownTextUpdate(completionWhenFinished: () -> () = { return}) {
        
        if (!upcomingUFCEvent.IsEventFinished()) {
            upcomingUFCEventCell.countdownLabel.text = "We Are Live!"
        } else {
            completionWhenFinished()
            upcomingUFCEventCell.countdownLabel.text = "Finished"
            upcomingUFCEventCell.backgroundColor = UIColor(red:0.19, green:0.16, blue:0.16, alpha:1.0)
        }
    }
    
    // MARK: - Data Setup & Custom Methods
    
    @objc func refreshView(sender:AnyObject) {
        refreshUFCEventsData()
        refreshControl.endRefreshing()
    }
    
    func setupUFCEventsData() {
        UFCEvent.fetchUFCEvents() { entities in
            if (entities.count == 0) {
                self.ufcEvents = UFCEvent.getAndSaveNewUFCEventsFromAPI()
                
                DispatchQueue.global(qos: .userInitiated).async {
                    for event in self.ufcEvents{
                        UFCEventFight.getAndSaveNewUFCFightsFromAPI(event: event)
                    }
                }
                if self.ufcEvents.count == 0 {
                    self.ufcEvents = entities
                }
            } else {
                self.ufcEvents = entities
                UFCEvent.checkForOldEventsAndRemoveWithChildEntites(byEvents: &self.ufcEvents)
                self.refreshUFCEventsData()
            }
            self.eventsTableView.reloadData()
        }
    }
    
    func refreshUFCEventsData() {
        DispatchQueue.global(qos: .background).async {
            UFCEvent.checkForOldEventsAndRemoveWithChildEntites(byEvents: &self.ufcEvents)
            
            let returnedEvents = UFCEvent.updateExistingEventsWithFreshDataFromAPI(events:
                self.ufcEvents)
            
            if returnedEvents.count > 0 {
                
                self.ufcEvents = returnedEvents
                DispatchQueue.main.async {
                    self.eventsTableView.reloadData()
                }
            }
        }
    }
}

// MARK: - TableView Data Source Extension

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ufcEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let currentUFCEvent = self.ufcEvents[indexPath.row];
        var cell: MainPageViewCell!
        
        if (indexPath.row == 0) {
            cell = self.eventsTableView.dequeueReusableCell(withIdentifier: "firstCell")!
                as? MainPageViewCell
        } else {
            cell = self.eventsTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)! as? MainPageViewCell
        }
        
        cell.titleLabel.text = "\(currentUFCEvent.baseTitle)\n\(currentUFCEvent.subTitle)"
        cell.dateLabel.text = "\(currentUFCEvent.localFullDateTimeDescription)"
        cell.locationLabel.text = "\(currentUFCEvent.location)\n\(currentUFCEvent.arena)"
        
        if (currentUFCEvent.isEventThisWeek) {
            
            upcomingUFCEvent = currentUFCEvent
            upcomingUFCEventCell = cell
            updateTimerWithCountdown(timer: nil)
            
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerWithCountdown(timer:)), userInfo: nil, repeats: true)
        } else {
            
            cell.countdownLabel.text = ""
            cell.dateLabel!.translatesAutoresizingMaskIntoConstraints = true
            cell.dateLabel.adjustsFontSizeToFitWidth = false
            
            var dateFrame = cell.dateLabel.frame
            dateFrame.origin.y = 95
            dateFrame.size.width = dateFrame.size.width + 5
            cell.dateLabel.frame = dateFrame
            
            cell.dateLabel.text = "\(currentUFCEvent.localDateString)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController")
        (controller as! EventDetailsViewController).ufcEvent = ufcEvents[indexPath.row]
        navigationController?.pushViewController(controller, animated: true)
        
        tableView.cellForRow(at: indexPath)?.isSelected = false
    
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
}

// MARK: - TableView Delegate Extension

extension ViewController: UITableViewDelegate {
    
}

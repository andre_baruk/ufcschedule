//
//  APICaller.swift
//  UFCSchedule
//
//  Created by Jay on 8/17/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Foundation

class APICaller{
    
    // MARK: - Static Properties
    
 static let base_URL = "http://ufc-data-api.ufc.com/api/"
 static let events_Endpoint = "v1/us/events"
 static let events_Fights_Endpoint = "v3/us/events"
 static let fighters_Endpoint = "v1/us/fighters"
    
 static let events_Etag = "Events_Etag"
 static let event_Fights_Etag_Template = "Event_Fights_Etag_"
 static let fighters_Etag = "Fighters_Etag"
 static let fighter_Etag_Template = "Fighter_Etag_"
    
    // MARK: - Public Methods
    
    static func getUFCEventsData(completion: (_ tempEvents: [[String : Any]] ) -> [[String : Any]] = { _ in  return []}) -> [[String : Any]] {
        
        var jsonData = APICaller.getJSONData(withEndpoint: events_Endpoint) { response,data,request  in
            
            let modifiedDate: String? = UserDefaults.standard.string(forKey: events_Etag)
            let newModifiedDate: String? = response?.allHeaderFields
                .filter({
                    ($0.key as? String) == "Etag"
                }).first?.value as? String
            
            let shouldProceedWithDataDownload =
                (modifiedDate == nil || newModifiedDate == nil || modifiedDate != newModifiedDate)
            
            if shouldProceedWithDataDownload {
                UserDefaults.standard.set(newModifiedDate, forKey: events_Etag)
                UserDefaults.standard.synchronize()
            }
            return shouldProceedWithDataDownload
        }
        
        let modifiedJsonModel = completion(jsonData)
        if (modifiedJsonModel.count > 0) {
            jsonData = modifiedJsonModel
        }
        return jsonData
    }
    
    static func getUFCEventFightsData(by eventId: Int) -> [[String : Any]]
    {
        let etagWithId = event_Fights_Etag_Template + String(eventId)
        
        let jsonData = APICaller
            .getJSONData(withEndpoint: events_Fights_Endpoint + "/" + String(eventId) + "/fights", httpMethod: "GET") { response, data, request in
                let modifiedDate: String? = UserDefaults.standard.string(forKey: etagWithId)
                let newModifiedDate: String? = response?.allHeaderFields
                    .filter({
                        ($0.key as? String) == "Etag"
                    }).first?.value as? String
                
                let shouldProceedWithDataDownload =
                    (modifiedDate == nil || newModifiedDate == nil || modifiedDate != newModifiedDate)
                
                if shouldProceedWithDataDownload {
                    UserDefaults.standard.set(newModifiedDate, forKey: etagWithId)
                    UserDefaults.standard.synchronize()
                }
                return shouldProceedWithDataDownload
        }
        return jsonData
    }
    
    static func getUFCFightersData() -> [[String : Any]] {
        var jsonData = APICaller
            .getJSONData(withEndpoint: fighters_Endpoint) { response, data, request in
                let modifiedDate: String? = UserDefaults.standard.string(forKey: fighters_Etag)
                let newModifiedDate: String? = response?.allHeaderFields
                    .filter({
                        ($0.key as? String) == "Etag"
                    }).first?.value as? String
                
                let shouldProceedWithDataDownload =
                    (modifiedDate == nil || newModifiedDate == nil || modifiedDate != newModifiedDate)
                
                if shouldProceedWithDataDownload {
                    
                    URLCache.shared.storeCachedResponse(CachedURLResponse(response: response!, data: data!), for: request!)
                    
                    UserDefaults.standard.set(newModifiedDate, forKey: fighters_Etag)
                    UserDefaults.standard.synchronize()
                }
                return shouldProceedWithDataDownload
        }
        
        if jsonData.count == 0 {
            jsonData = FightersCacheStore
        } else {
            FightersCacheStore = getFightersCache()
        }
        
        return jsonData
    }
    
    static func getUFCFighterData(by fighterId: Int) -> [[String : Any]]
    {
        let etagWithId = fighter_Etag_Template + String(fighterId)
        
        let jsonData = APICaller
            .getJSONData(withEndpoint: fighters_Endpoint + "/" + String(fighterId), httpMethod: "POST") { response, data, request in
                let modifiedDate: String? = UserDefaults.standard.string(forKey: etagWithId)
                let newModifiedDate: String? = response?.allHeaderFields
                    .filter({
                        ($0.key as? String) == "Etag"
                    }).first?.value as? String
                
                let shouldProceedWithDataDownload =
                    (modifiedDate == nil || newModifiedDate == nil || modifiedDate != newModifiedDate)
                
                if shouldProceedWithDataDownload {
                    UserDefaults.standard.set(newModifiedDate, forKey: etagWithId)
                    UserDefaults.standard.synchronize()
                }
                return shouldProceedWithDataDownload
        }
        return jsonData
    }
    
    // MARK: - Cache Methods
    
    static var FightersCacheStore = getFightersCache()
    private static func getFightersCache() -> [[String : Any]]
    {
        let fullURL = base_URL + fighters_Endpoint
        let request = URLRequest(url: URL(string: fullURL)!)
        let cachedResponse = URLCache.shared.cachedResponse(for: request)
        var json: [[String: Any]]!
        do{
            json = try ((JSONSerialization.jsonObject(with: (cachedResponse?.data)!, options: []) as? [[String: Any]]))
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        return json
    }
    
    /*
    static func testCache()
    {
        let fullURL = Base_URL + Events_Endpoint
        var request = URLRequest(url: URL(string: fullURL)!)
        let tempToCheck = URLCache.shared.cachedResponse(for: request)
        var json: [[String: Any]]!
        do{
        json = try JSONSerialization.jsonObject(with: (tempToCheck?.data)!, options: []) as! [[String: Any]]
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    */
    
    // MARK: - Private Methods
    
    private static func getJSONData(withEndpoint endpoint: String = "", httpMethod: String = "GET", responseDataHandler: @escaping (HTTPURLResponse?, Data?, URLRequest?) -> (Bool) = {_,_,_  in return true}) -> [[String: Any]]{
        let fullURL = base_URL + endpoint
        var request = URLRequest(url: URL(string: fullURL)!)
        request.httpMethod = httpMethod
       // request.setValue("Accept", forHTTPHeaderField: "application/json")
        let sem = DispatchSemaphore(value: 0)
        var json: [[String: Any]] = []
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print("error=\(String(describing: error))")
                return
            }

            if (responseDataHandler((response as? HTTPURLResponse), data, request) == true) {
                do {
                    json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }

                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
            }
            sem.signal()
        }
        task.resume()

        _ = sem.wait(timeout: .distantFuture)
        return json
    }
        

}


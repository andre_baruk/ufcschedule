//
//  EventDetailsViewController.swift
//  UFCSchedule
//
//  Created by Jay on 8/17/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController {
    
    var ufcEvent: UFCEvent!
    let cellReuseIdentifier_fightCell = "fightCell"
    var fighterImagesDictionaryCache: [Int32 : (UIImage, UIImage)] = [:]
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var fightsTableView: UITableView!
    
    // MARK: - View Methods
    
    override func viewDidLoad() {
        setupNavigationBar()
        
        fightsTableView.delegate = self
        fightsTableView.dataSource = self
       // fightsTableView.register(UINib(nibName: "FightCellView", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier_fightCell)
        fightsTableView.separatorColor = .clear
        
        //  Pull down to refresh setup
        refreshControl.addTarget(self, action: #selector(refreshView(sender:)), for: UIControl.Event.valueChanged)
        fightsTableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "FightsTableHeaderView", bundle: nil)
        let header = nib.instantiate(withOwner: self, options: nil).first as! FightsTableHeaderView
        header.eventTitleLabel.text = "\(ufcEvent.baseTitle) \n \(ufcEvent.subTitle)"
        
        fightsTableView.parallaxHeader.view = header
        fightsTableView.parallaxHeader.height = 50
        fightsTableView.parallaxHeader.minimumHeight = 0
        fightsTableView.parallaxHeader.mode = .topFill
        
        fightsTableView.backgroundColor = .ufcDarkRed
    }
    
    func setupNavigationBar() {
        let navController = self.navigationController!
        navController.setNavigationBarHidden(false, animated: true)
        
        navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.isTranslucent = true
        
        navController.navigationBar.tintColor = UIColor(red:0.82, green:0.07, blue:0.10, alpha:1.0)
    }
    
    // MARK: - Data Setup & Custom Methods
    
    func setFighterImageAsync(cell: FightCellView, fighter1Id: Int32, fighter2Id: Int32, fightId: Int32) {
        
        DispatchQueue.global(qos: .background).async {
            
            let image1 = FighterImageHelper.getFighterImage(by: Int(fighter1Id))
            let image2 = FighterImageHelper.getFighterImage(by: Int(fighter2Id))
            DispatchQueue.main.async {
                
                if cell.tag == fightId {
                    cell.fighter1Image.image = image1
                    cell.fighter2Image.image = image2
                    cell.fighter1Image.roundCorners()
                    cell.fighter2Image.roundCorners()
                    self.fighterImagesDictionaryCache[fightId] = (image1, image2)
                }
            }
        }
    }
    
    @objc func refreshView(sender:AnyObject) {
        refreshFightsData()
        refreshControl.endRefreshing()
    }
    
    func refreshFightsData() {
        DispatchQueue.global(qos: .background).async {
            let returnedEvents = UFCEventFight.getUFCEventFightsFromAPIAndUpdateExisting(event: self.ufcEvent!)
            
            if returnedEvents.count > 0 {
                
                DispatchQueue.main.async {
                    self.fightsTableView.reloadData()
                    print("reloaded")
                }
            }
        }
    }
}

// MARK: - TableView Data Source Extension

extension EventDetailsViewController: UITableViewDataSource {
    
    /*
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.ufcSelectedRed
        
        let sectionLabel = UILabel(frame: CGRect(x: 200, y: 5, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        sectionLabel.font = UIFont(name: "Helvetica-Bold", size: 16)
        sectionLabel.textColor = UIColor.black
        sectionLabel.text = "NETWORK SETTINGS"
        sectionLabel.sizeToFit()
        headerView.addSubview(sectionLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ufcEvent.eventFights.count
    }
    
    static var cellCounter = 0
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let currentFight = ufcEvent.orderedEventFights[indexPath.row]
        var cell: FightCellView!
        
        let cellIdentifier = String.init(format: "%@%d%d", cellReuseIdentifier_fightCell, indexPath.section, indexPath.row)
        
            cell = self.fightsTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FightCellView

        if cell == nil {
            fightsTableView.register(UINib(nibName: "FightCellView", bundle: nil), forCellReuseIdentifier: cellIdentifier)
            cell = self.fightsTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? FightCellView
        }
        
        cell.fighter1NameLabel.text = currentFight.fighter1Name
        cell.fighter2NameLabel.text = currentFight.fighter2Name

        cell.tag = Int(currentFight.fightId)
        
        if let imgs = fighterImagesDictionaryCache[currentFight.fightId] {
            cell.fighter1Image.image = imgs.0
            cell.fighter2Image.image = imgs.1
            
            cell.fighter1Image.roundCorners()
            cell.fighter2Image.roundCorners()
        } else {
            setFighterImageAsync(cell: cell, fighter1Id: currentFight.fighter1Id, fighter2Id: currentFight.fighter2Id, fightId: currentFight.fightId)
        }

        return cell
    }
}

// MARK: - TableView Delegate Extension

extension EventDetailsViewController: UITableViewDelegate {
    
}

// MARK: - FightsTableHeaderView Class

class FightsTableHeaderView: UIView {
    @IBOutlet weak var eventTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        eventTitleLabel.sizeToFit()
        eventTitleLabel.adjustsFontSizeToFitWidth = true
    }
}

// MARK: - Legacy Code

/*

// mark: - Container View Setup

override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "containerSegue" {
        contentViewController = segue.destination as UIViewController
        
        titleLabelView = ((contentViewController?.view.subviews[0])! as! UILabel);
        dateLabelView = ((contentViewController?.view.subviews[1])! as! UILabel);
        locationLabelView = ((contentViewController?.view.subviews[2])! as! UILabel);
    }
}
 */


//
//  Helper+Extensions.swift
//  UFCSchedule
//
//  Created by Jay on 10/26/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit


extension UIColor {
    
    class var ufcDarkRed: UIColor {
        return UIColor(red:0.34, green:0.07, blue:0.07, alpha:1.0)
    }
    
    class var ufcSelectedRed: UIColor {
        return UIColor(red:0.82, green:0.07, blue:0.10, alpha:1.0)
    }
}

extension UIImage {
    
    func resizeImageWith(newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        self.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func crop(to:CGSize) -> UIImage {
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect.init(x: posX, y: posY, width: cropWidth, height: cropHeight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, true, self.scale)
        cropped.draw(in: CGRect.init(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized!
    }
    
}

extension UIImageView {
    func roundCorners() {
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}

class FighterImageHelper {
    
    static func getFighterImage(by id: Int) -> UIImage {
        let fightersData = APICaller.getUFCFightersData()
        
        let fighterThumbnail = fightersData.filter({
            ($0["id"] as? Int) == id
        }).map({ dict -> UIImage? in
            let string = dict["thumbnail"] as? String
            if let string = string {
                let url = URL.init(string: string)
                let data = NSData.init(contentsOf: url!)
                let image = UIImage.init(data: data! as Data)
                return image
            }
            return nil
        }).first
        
        if let fighterThumbnailValue = fighterThumbnail.flatMap({ $0 }) {
            return fighterThumbnailValue
        }
        
        return UIImage()
    }
}

import CoreData

class CoreDataHelper: NSObject {
    static let sharedInstance = CoreDataHelper()
    let container: NSPersistentContainer!
    
    private override init() {
        let container = NSPersistentContainer(name: "ModelBase")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error \(error), \(String(describing: error._userInfo))")
            }
        })
        
       // let directory = NSPersistentContainer.defaultDirectoryURL()
       // let url = directory.appendingPathComponent("ModelBase" + ".sqlite")
        //  print("My SQL Location")
        //   print (url)
        
        self.container = container
        
        super.init()
    }
}

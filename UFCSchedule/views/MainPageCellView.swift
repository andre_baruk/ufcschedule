//
//  MainPageCellViewController.swift
//  UFCSchedule
//
//  Created by Jay on 8/26/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit

class MainPageViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
        
        titleLabel.textColor = UIColor.white
        locationLabel.textColor = UIColor.white
        countdownLabel.textColor = UIColor.white
        dateLabel.textColor = UIColor.white
        
        titleLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
        locationLabel.font = UIFont(name: "HelveticaNeue-Italic", size: 14.0)
        dateLabel.font = UIFont(name: "HelveticaNeue-Italic", size: 14.0)
        countdownLabel.font = UIFont(name: "HelveticaNeue-BoldItalic", size: 15.0)
        
        locationLabel.sizeToFit()
        locationLabel.adjustsFontSizeToFitWidth = true
        titleLabel.sizeToFit()
        titleLabel.adjustsFontSizeToFitWidth = true
        countdownLabel.sizeToFit()
        countdownLabel.adjustsFontSizeToFitWidth = true
        dateLabel.sizeToFit()
        dateLabel.adjustsFontSizeToFitWidth = true
    }
    
}

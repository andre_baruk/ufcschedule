//
//  FightCellView.swift
//  UFCSchedule
//
//  Created by Jay on 10/26/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import UIKit

class FightCellView: UITableViewCell {

    @IBOutlet weak var fighter1NameLabel: UILabel!
    @IBOutlet weak var fighter2NameLabel: UILabel!
    @IBOutlet weak var vsLabel: UILabel!
    @IBOutlet weak var fighter1Image: UIImageView!
    @IBOutlet weak var fighter2Image: UIImageView!
    @IBOutlet weak var titleFightLogoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
        
        fighter1NameLabel.textColor = UIColor.white
        fighter2NameLabel.textColor = UIColor.white
        vsLabel.textColor = UIColor.white
        
        fighter1NameLabel.font = UIFont(name: "HelveticaNeue-BoldItalic", size: 14.0)
        fighter2NameLabel.font = UIFont(name: "HelveticaNeue-BoldItalic", size: 14.0)
        vsLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 16.0)

        fighter1NameLabel.sizeToFit()
        fighter1NameLabel.adjustsFontSizeToFitWidth = true
        fighter2NameLabel.sizeToFit()
        fighter2NameLabel.adjustsFontSizeToFitWidth = true
        
        fighter1Image.contentMode = .scaleAspectFit
    }
}
